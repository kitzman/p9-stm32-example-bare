/* general constants */
static int hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7',
						'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

/* clock constants */
#define FCLK 128000000UL

/*	GPIO configuration and definitions	*/
typedef
enum
{
	GpioPortA,
	GpioPortB,
	GpioPortC,
	GpioPortD,
} GpioPort;

typedef
struct GpioLine
{
	GpioPort	port;
	char		pin;
	char		mode;
} GpioLine;

/*	USART configuration and definitions	*/
typedef
enum
{
	Usart1,
	Usart2,
} UsartLine;

/*	DMA definitions	*/
#define DMA_BUFSIZE		32U
typedef
enum
{
	DmaChan1,
	DmaChan2,
	DmaChan3,
	DmaChan4,
	DmaChan5,
	DmaChan6,
	DmaChan7,
} DmaChannel;

/*	synchronization	*/
/*typedef
struct Lock {
	int	val;
} Lock;*/
