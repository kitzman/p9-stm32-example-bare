#include	<u.h>
#include	"mem.h"
#include	"dat.h"
#include	"fns.h"
#include	"libkern/kern.h"

#define PRINTSIZE	256

int
sprint(char *s, char *fmt, ...)
{
	int n;
	va_list arg;

	va_start(arg, fmt);
	n = vseprint(s, s+PRINTSIZE, fmt, arg) - s;
	va_end(arg);

	return n;
}

int
print(char *fmt, ...)
{
	int n;
	va_list arg;
	char *t;

	char buf[PRINTSIZE];

	va_start(arg, fmt);
	n = vseprint(buf, buf+sizeof(buf), fmt, arg) - buf;
	va_end(arg);

	t = buf;
    for (; ((uint)t - (uint)buf) < n; t++) {
		uart1_putc((int)*t);
	}

	return n;
}

int
fprint(int fd, char *fmt, ...)
{
	int n;
	va_list arg;
	return -1;
/*
	if(_fds[fd] == nil)
		return -1;

	char buf[PRINTSIZE];

	USED(fd);
	va_start(arg, fmt);
	n = vseprint(buf, buf+sizeof(buf), fmt, arg) - buf;
	va_end(arg);

	char *t = buf;
    for (; ((uint)t - (uint)buf) < n; t++) {
		uart1_putc((int)*t);
	}
*/
	return n;
}
