#include	<u.h>
#include	"mem.h"
#include	"dat.h"
#include	"fns.h"
#include	"include/stm32f103xb.h"
#include	"libkern/kern.h"

void
main()
{
	/* initialize system */
	clockinit();
	allocinit();
	dmainit();

	/* initialize peripherals and enable the DMA */
	gpioinit();
	afioinit();
	uartinit();

	dmaen();

	/* entrypoint */
	for(int i = 0; i < 3; i++)
		gpio_toggle_n(3);

	print("system booting					kitzman unlimited (c) 2023\n");
	print("basic functions working\n");

	GpioLine l;
	l.port = GpioPortC;
	l.pin = 13;

	uint count = 0;
	uint uc = 0;
	char buf[128];
	// char msg[32] = "hello!\n";

	while(1) {
		gpio_set(l, 0);
		_wait(1000000);
		gpio_set(l, 1);
		_wait(500000);
		print("main: cycle 0x%04x\n", count);
		uc = dma_read(DmaChan6, buf, 128);
		if(uc > 0) {
			print("main: usart2 got %ud bytes\n", uc);
			uc = 0;
		}
		// dma_write(DmaChan7, msg, strlen(msg));
		count++;
	}
}
