</sys/src/mkfile.proto
<mkconfig

BIN=bin
loadaddr=0x08000000
dataaddr=0x20000000

LIBS=\
	kern\

TARG=\
	$BIN/kernel\
	$BIN/kernel.asm\

OFILES=\
	div-thumb.$O\
	thumb2.$O\
	clock.$O\
	dma.$O\
	gpio.$O\
	uart.$O\
	alloc.$O\
	sub.$O\
	print.$O\
	main.$O\

HFILES=\
	include/stdint.h\
	include/core_cm3.h\
	include/system_stm32f1xx.h\
	include/stm32f103xb.h\
	libkern/kern.h\
	thumb2.h\
	fns.h\
	dat.h\
	mem.h\
	debug.h\
	handlers.h\

LIBFILES=${LIBS:%=bin/lib%.a}

all:V: kernel

bin:V:
	mkdir -p bin

clean:V:
	rm -rf $TARG *.$O

%.$O:	%.s
	$AS $ASFLAGS $stem.s

%.$O:	%.c
	$CC $CFLAGS $stem.c

%.$O:	$HFILES

kernel.asm kernel: l.$O $OFILES $LIBFILES
	$LD -a $LDFLAGS -o $BIN/kernel -R0 -T$loadaddr -D$dataaddr l.$O $OFILES $LIBFILES >$BIN/kernel.asm

$BIN/%:	%
	cp $stem $BIN/$stem
