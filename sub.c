#include	<u.h>
#include	"mem.h"
#include	"dat.h"
#include	"fns.h"
#include	"libkern/kern.h"

#define PRINTSIZE	256

/*
 * read/write operations
 */

void
panic(char *fmt, ...)
{
	int n;
	va_list arg;
	char *t;

	if(fmt != nil) {
		char buf[PRINTSIZE];

		va_start(arg, fmt);
		n = vseprint(buf, buf+sizeof(buf), fmt, arg) - buf;
		va_end(arg);

		t = buf;
		for (; ((uint)t - (uint)buf) < n; t++) {
			uart1_putc((int)*t);
		}
	}

	while(1) gpio_toggle_n(2);
}

/*
 * Checksum and formatting
 */

static void
hexfmt(char *s, int i, ulong a)
{
	s += i;
	while(i > 0){
		*--s = hex[a&15];
		a >>= 4;
		i--;
	}
}

static int
checksum(void *v, int n)
{
	uchar *p, s;

	s = 0;
	p = v;
	while(n-- > 0)
		s += *p++;
	return s;
}

