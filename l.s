#include	"mem.h"
#include	"handlers.h"
#include	"thumb2.h"

#define _estack	DATAEADDR			// the stack starts at DATAEADDR
#define RAMBOOT	0xF108F85F

#undef ORB

THUMB=4

/*	vector table 0x08000000-0x08000108	*/
TEXT _vector_table(SB), $0
	WORD	$_estack
	/*	core exceptions */
	WORD	$Reset_Handler
	WORD	$NMI_Handler
	WORD	$HardFault_Handler
	WORD	$MemManage_Handler
	WORD	$BusFault_Handler
	WORD	$UsageFault_Handler
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$SVC_Handler
	WORD	$DebugMon_Handler
	WORD	$0x00
	WORD	$PendSV_Handler
	WORD	$SysTick_Handler
	/*	external exceptions	*/
	WORD	$WWDG_IRQHandler
	WORD	$PVD_IRQHandler
	WORD	$TAMPER_IRQHandler
	WORD	$RTC_IRQHandler
	WORD	$FLASH_IRQHandler
	WORD	$RCC_IRQHandler
	WORD	$EXTI0_IRQHandler
	WORD	$EXTI1_IRQHandler
	WORD	$EXTI2_IRQHandler
	WORD	$EXTI3_IRQHandler
	WORD	$EXTI4_IRQHandler
	WORD	$DMA1_Channel1_IRQHandler
	WORD	$DMA1_Channel2_IRQHandler
	WORD	$DMA1_Channel3_IRQHandler
	WORD	$DMA1_Channel4_IRQHandler
	WORD	$DMA1_Channel5_IRQHandler
	WORD	$DMA1_Channel6_IRQHandler
	WORD	$DMA1_Channel7_IRQHandler
	WORD	$ADC1_2_IRQHandler
	WORD	$USB_HP_CAN1_TX_IRQHandler
	WORD	$USB_LP_CAN1_RX0_IRQHandler
	WORD	$CAN1_RX1_IRQHandler
	WORD	$CAN1_SCE_IRQHandler
	WORD	$EXTI9_5_IRQHandler
	WORD	$TIM1_BRK_IRQHandler
	WORD	$TIM1_UP_IRQHandler
	WORD	$TIM1_TRG_COM_IRQHandler
	WORD	$TIM1_CC_IRQHandler
	WORD	$TIM2_IRQHandler
	WORD	$TIM3_IRQHandler
	WORD	$TIM4_IRQHandler
	WORD	$I2C1_EV_IRQHandler
	WORD	$I2C1_ER_IRQHandler
	WORD	$I2C2_EV_IRQHandler
	WORD	$I2C2_ER_IRQHandler
	WORD	$SPI1_IRQHandler
	WORD	$SPI2_IRQHandler
	WORD	$USART1_IRQHandler
	WORD	$USART2_IRQHandler
	WORD	$USART3_IRQHandler
	WORD	$EXTI15_10_IRQHandler
	WORD	$RTC_Alarm_IRQHandler
	WORD	$USBWakeUp_IRQHandler
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	WORD	$0x00
	/*	ram boot	*/
	WORD	$0x00

/*	startup section	*/
TEXT _start(SB), THUMB, $-4
	MOVW    $setR12(SB), R1
	MOVW    R1, R12	/* static base (SB) */

	MOVW    $DATAEADDR, R1
	MOVW	R1, SP

	// copy the text segment unto the data segment
    MOVW    $etext(SB), R1
    MOVW    $bdata(SB), R2
    MOVW    $edata(SB), R3

_start_loop:
    CMP     R3, R2
    BGE     _end_start_loop

    MOVW    (R1), R4
    MOVW    R4, (R2)
    ADD     $4, R1
    ADD     $4, R2
    B       _start_loop

_end_start_loop:
//	B		,introff(SB)
	B		,main(SB)

/*	Interrupt handlers	*/

/*	default handler	*/
TEXT _default_handler(SB), THUMB, $-4
_infinite_loop:
	MOVW	SP, R0
	B		_infinite_loop
