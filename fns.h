/*	l.s 	*/
void	start(void *sp);

/*	main.c 	*/
void	main(void);

/*	interrupt handlers 	*/
void	_reset_handler(void);
void	_default_handler(void);

/*	thumb2.s extra 	*/
void	introff(void);
void	intron(void);

/*	div.s 	*/
void	_div(void);
void	_divu(void);
void	_mod(void);
void	_modu(void);

/*	clock.c 	*/
void	clockinit(void);
void	clockdeinit(void);
void	_wait(ulong);

/*	sub.c 	*/
void	panic(char*, ...);

/*	alloc.c 	*/
void	allocinit(void);
void*	smalloc(ulong);
void*	malloc(ulong);
void*	mallocz(ulong, int);
// void*	mallocalign(ulong size, ulong align, long offset, ulong span);
void	free(void*);
void*	realloc(void*, ulong);
// ulong	msize(void *v);
// void	setmalloctag(void *v, uintptr pc);
// void	setrealloctag(void *v, uintptr pc);
// uintptr	getmalloctag(void *v);
// uintptr	getrealloctag(void *v);

/*	dma.c 	*/
void	dmainit(void);
void	dmaen(void);

void	dma_write(DmaChannel, void*, ulong);
ulong	dma_read(DmaChannel, void*, ulong n);
//void	dma_readn(DmaChannel, void, ulong n);

/*	gpio.c 	*/
void	gpioinit(void);
void	afioinit(void);
int		gpio_set(GpioLine, uint);
void	gpio_toggle_n(int);

/*	uart.c 	*/
void	uartinit(void);
void	uart1_putc(int);
int		uart1_getc(void);
void	uart2_putc(int);
int		uart2_getc(void);
