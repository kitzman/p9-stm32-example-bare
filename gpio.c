#include	<u.h>
#include	"dat.h"
#include	"fns.h"
#include	"include/stm32f103xb.h"

void
gpioinit()
{
	// PC13 output (LED)
	GPIOC->CRH &= ~(GPIO_CRH_MODE13 | GPIO_CRH_CNF13);
	GPIOC->CRH |= GPIO_CRH_MODE13_0 | GPIO_CRH_MODE13_1;
}

void
afioinit()
{
	/* UsartLine1 */
	GPIOA->CRH &= ~(GPIO_CRH_MODE9 | GPIO_CRH_CNF9);	// TX
	GPIOA->CRH &= ~(GPIO_CRH_MODE10 | GPIO_CRH_CNF10);	// RX
	GPIOA->CRH |= GPIO_CRH_MODE9_0 |
					GPIO_CRH_MODE9_1 |
					GPIO_CRH_CNF9_1;	// TX AF PP
	GPIOA->CRH |= GPIO_CRH_CNF10_0 |
					GPIO_CRH_CNF10_1;	// RX AF input pull-up

	/* UsartLine2 */
	GPIOA->CRL &= ~(GPIO_CRL_MODE2 | GPIO_CRL_CNF2);	// TX
	GPIOA->CRL &= ~(GPIO_CRL_MODE3 | GPIO_CRL_CNF3);	// RX
	GPIOA->CRL |= GPIO_CRL_MODE2_0 |
					GPIO_CRL_MODE2_1 |
					GPIO_CRL_CNF2_1;	// TX AF PP
	GPIOA->CRL |= GPIO_CRL_CNF3_0 |
					GPIO_CRL_CNF3_1;	// RX AF input pull-up
}

int
gpio_set(GpioLine l, uint v)
{
	GPIO_TypeDef *gpio;
	uint pin_pos = 0;

	if(v > 1)
		return -1;

	pin_pos = (uint)l.pin;

	switch(l.port) {
	case GpioPortA:	gpio = GPIOA; break;
	case GpioPortB:	gpio = GPIOB; break;
	case GpioPortC: gpio = GPIOC; break;
	case GpioPortD:	gpio = GPIOD; break;
	default:		return -1;
	}

	switch(v) {
	case 0:		gpio->BSRR |= (1U << pin_pos) << 0x10; break;
	case 1: 	gpio->BSRR |= 1U << pin_pos; break;
	default:	return -1;
	}

	return 0;
}

void
gpio_toggle_n(int n)
{
	GpioLine l;
	l.port = GpioPortC;
	l.pin = 13;
	for(int i = 0; i < n; i++) {
		gpio_set(l, 0);
		_wait(100000);
		gpio_set(l, 1);
		_wait(100000);
	}
	_wait(150000);	
}

