#include	<u.h>
#include	"mem.h"
#include	"dat.h"
#include	"fns.h"
#include	"include/stm32f103xb.h"
#include	"libkern/kern.h"

/*	usart initialization for USART1 and USART2	*/
/*		usartdiv = (FCLK / (8UL * 2UL)) / USART_BAUD;
		mantissa = floor(usartdiv)
		fraction = floor((usartdiv - floor(usartdiv)) * 16) 
		Fclk = AHB = 48MHz	*/
void
uartinit()
{
	// USART1 19200 8E1 TX
	USART1->BRR = 0x4E2;
	USART1->CR1 = USART_CR1_M | USART_CR1_PCE;
	USART1->CR2 = 0;
	USART1->CR1 |= USART_CR1_TE;

	// USART2 115200 8N1 RX/TX
	USART2->BRR = 0xD0;
	USART2->CR1 = 0;
	USART2->CR2 = 0;
	USART2->CR3 = USART_CR3_DMAR | USART_CR3_DMAT;
	USART2->CR1 |= USART_CR1_RE | USART_CR1_TE;

	// enable USART
	USART1->CR1 |= USART_CR1_UE;
	USART2->CR1 |= USART_CR1_UE;
}

/*	uart getc and putc	*/

void
uart1_putc(int c)
{
	USART1->DR = c;
	while(!(USART1->SR & USART_SR_TC));
}

int
uart1_getc()
{
	return -1;
}

void
uart2_putc(int c)
{
	USART2->DR = c;
	while(!(USART2->SR & USART_SR_TC));
}
